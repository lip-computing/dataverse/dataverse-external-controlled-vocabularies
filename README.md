# Polen Dataverse External Controlled Vocabularies

Repository with the configuration files used in Polen Repository to implement external control vocabularies.

Run the following command to add the `polen_cvocs.json` to the dataverse installation:

```bash
curl -X PUT --upload-file config/polen_cvocs.json http://localhost:8080/api/admin/settings/:CVocConf
```

Javascripts should be added to payara dataverse files and ´js-url´ should be changed according to dataverse installation url and the path where files are moved to (example: `https://example.com/affiliation.js`).