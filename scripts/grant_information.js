/************************************************************************************************************
 * This JavaScript is responsible for the controlled vocabulary features in the browser.                    *
 * The Grant Information fields will be prefilled with info retrieved from SciProj API and that relate to   *
 * the parent dataverse identifier (Project ID).                                                            *
 * ******************************************************************************************************** *
 * Authors: César Ferreira and João Machado @ INCD, Zacarias Benta @ LIP                                    *                                                                                                  *
 ************************************************************************************************************/

/* DOM Classes
 * ***********
 * incd_grant_information: class added when the Grant Information values are prefilled by the script.
 */

$(document).ready(function() {
  // Remove add/delete field button
  removeButton();
  // Get the dataverse parent id
  var parentId = getParentId();
  console.log("This is my js: " + parentId);
  // Fill field with info from SciProj API
  fillFundingInformation(parentId);
});

var grantSelector = "div > div#metadata_grantNumber ~ div.dataset-field-values div.edit-compound-field";
var parentDataverseSelector = "body nav div#breadcrumbNavBlock";
var addDeleteButtonSelector = "div > div#metadata_grantNumber ~ div.dataset-field-values div.field-add-delete";

function removeButton(){
  element = document.querySelector(addDeleteButtonSelector)
  if (!element.classList.contains('incd_grant_information')) {
    element.classList.add('incd_grant_information');
    element.remove();
    console.log("Button Removed")
  }
}

function getParentId(){
  var element = document.querySelector(parentDataverseSelector)
  var elementChildSize = element.childElementCount;
  // The parent URL is in the 3rd position of child Elements, starting from the end
  var parentUrl = new URL(element.children[elementChildSize-3].children[0].href);
  // parse url
  var pathname = parentUrl.pathname;
  // Remove trailing slashes if present
  pathname = pathname.replace(/\/$/, "");
  // Split the pathname by slashes
  var segments = pathname.split("/");
  // Extract the last element
  parentId = segments[segments.length - 1];
  return parentId;
}

function fillFundingInformation(parentId){
  let sciProjUrl = "http://localhost:5000/api/project?id=" + parentId;
  fetch(sciProjUrl, { headers: {'Access-Control-Allow-Origin' : '*'} })
  .then(response => response.json())
  .then(data => {
    // Store Funding Information
    grantAgencyValue = data['OAI-PMH']['ListRecords']['record']['metadata']['Project']['Funded']['By']['OrgUnit']['Name']['$'];
    console.log("This is my js: " + grantAgencyValue);
    grantIdentifierValue = data['OAI-PMH']['ListRecords']['record']['metadata']['Project']['Funded']['By']['OrgUnit']['Identifier']['$'];
    console.log("This is my js: " + grantIdentifierValue);
    fillFundingFields(grantAgencyValue, grantIdentifierValue);
  });
}

function fillFundingFields(grantAgencyValue, grantIdentifierValue) {
  // Iterate over compound elements
  document.querySelectorAll(grantSelector).forEach(element => {
    // 'incd_grant_information' class marks elements that have already been processed
    if (!element.classList.contains('incd_grant_information')) {
      element.classList.add('incd_grant_information');
      // Grant Agency is the first child element and Grant Identifier is the second
      // The second children index 1 corresponds to the input box
      let grantAgencyField = element.children[0].children[1];
      let grantIdentifierField = element.children[1].children[1];
      // Fill both fields
      grantAgencyField.value = grantAgencyValue;
      grantIdentifierField.value = grantIdentifierValue;
    }
  });
}