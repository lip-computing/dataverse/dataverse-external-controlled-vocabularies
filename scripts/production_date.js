/************************************************************************************************************
 * This JavaScript is responsible for the controlled vocabulary features in the browser.                    *
 * The Production Date field will be prefilled with the same value as in Deposit Date field in the dataset  *
 * creation form.                                                                                           *
 * ******************************************************************************************************** *
 * Authors: César Ferreira and João Machado @ INCD, Zacarias Benta @ LIP                                    *                                                                                                  *
 ************************************************************************************************************/


/* DOM Classes
 * ***********
 * incd_production_date: class added when the Production Date value is filled with Deposit Date value.
 */

var productionDate = "YYYY-MM-DD";

$(document).ready(function() {
  // Set date to the current day
  dateInit();

  // Change productionDate to the value in dateOfDeposit
  getdateOfDeposit();

  // fill productionDate
  fillProductionDate(productionDate);
});


var productionDateSelector = "span[data-original-title='The date when the data were produced (not distributed, published, or archived)']";
var dateOfDepositSelector = "span[data-original-title='The date when the Dataset was deposited into the repository']";


function dateInit() {
  const today = new Date();

  let day = today.getDate();
  let month = today.getMonth() + 1;
  let year = today.getFullYear();

  // This arrangement can be altered based on how we want the date's format to appear.
  productionDate = `${year}-${month}-${day}`;

}

function fillProductionDate(productionDate) {
  document.querySelectorAll(productionDateSelector).forEach(element => {
    // 'incd_production_date' class marks elements that have already been processed
    if (!element.classList.contains('incd_production_date')) {
      element.classList.add('incd_production_date');
      // Fill-in productionDate
      let productionDateElementId = document.querySelector(productionDateSelector).parentElement.parentElement.nextElementSibling.nextElementSibling.childNodes[1].childNodes[1].childNodes[1].childNodes[1].childNodes[0].id
      let productionDateElement = document.getElementById(productionDateElementId);
      productionDateElement.value = productionDate;
    }
  })
}

function getdateOfDeposit() {
  // Iterate over compound elements
  document.querySelectorAll(dateOfDepositSelector).forEach(element => {
    // 'incd_production_date' class marks elements that have already been processed
    if (!element.classList.contains('incd_production_date')) {
      element.classList.add('incd_production_date');
      let dateOfDepositField = element;
      // Input field within
      productionDate = document.querySelector(dateOfDepositSelector).parentElement.parentElement.nextElementSibling.nextElementSibling.childNodes[1].childNodes[1].childNodes[1].childNodes[1].childNodes[0].value;
    }
  })
}

